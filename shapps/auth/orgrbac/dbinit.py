#! /usr/bin/env python
#coding=utf-8

from uliweb import functions
from uliweb.orm import get_model
from sqlalchemy.sql import and_

def test():
    RbacOrg = get_model('rbacorg')
    RbacScheme = get_model('rbacscheme')
    Role = get_model('role')
    Perm = get_model('permission')
    User = get_model('user')
    OrgRole = get_model('orgrole')
    
    users = [{"username":"user1"},
        {"username":"user2"}
    ]
    for user in users:
        username = user["username"]
        user = User.get(User.c.username==username)
        if not user:
            user = User(username=username)
            print "Add user %s"%(username)
            user.save()
    
    schemes = [
        {'name':'scheme1'},
        {'name':'scheme2'},
    ]
    
    for scheme in schemes:
        name = scheme["name"]
        rbacscheme = RbacScheme.get(RbacScheme.c.name==name)
        if not rbacscheme:
            print "Add scheme %s"%(name)
            rbacscheme = RbacScheme(name=name)
            rbacscheme.save()
    
    orgs = [
        {'name':'org1','scheme':'scheme1'},
        {'name':'org2','scheme':'scheme2'},
    ]
    for org in orgs:
        name = org["name"]
        rbacorg = RbacOrg.get(RbacOrg.c.name==name)
        if not rbacorg:
            scheme = RbacScheme.get(RbacScheme.c.name==org["scheme"])
            print "Add rbacorg %s %s"%(name,scheme)
            rbacorg = RbacOrg(name=name,rbacscheme=scheme)
            rbacorg.save()
    
    role1 = Role.get(Role.c.name=="role1")
    org1 = RbacOrg.get(RbacOrg.c.name=="org1")
    org1role1 = OrgRole.get(and_(OrgRole.c.role==role1.id,OrgRole.c.organization==org1.id))
    if not org1role1:
        org1role1 = OrgRole(role=role1,organization=org1)
        org1role1.save()
    ul = list(org1role1.users.all())
    if not ul:
        print "org1role1 add user1"
        org1role1.users.add(User.get(User.c.username=="user1"))
        ul = list(org1role1.users.all())
    print "org1role1.users:",ul
    
def test_add_Role_Perm_Rel():
    Role_Perm_Rel = get_model('role_perm_rel')
    Role = get_model('role')
    Perm = get_model('permission')
    RbacScheme = get_model('rbacscheme')
    Role_Perm_Rel_list = [
        ('scheme1','perm1','role1'),
        ('scheme1','perm1','role2'),
        ('scheme2','perm1','role1'),
        ('scheme2','perm2','role1')
    ]
    if Role_Perm_Rel.all().count()>0:
        return
    for rbacscheme,perm,role in Role_Perm_Rel_list:
        rbacscheme = RbacScheme.get(RbacScheme.c.name==rbacscheme)
        perm = Perm.get(Perm.c.name==perm)
        role = Role.get(Role.c.name==role)
        print "Add Role_Perm_Rel(%s %s %s)"%(rbacscheme,perm,role)
        Role_Perm_Rel(role=role,permission=perm,scheme=rbacscheme).save()

def test_query_perm():
    Role = get_model('role')
    Perm = get_model('permission')
    RbacOrg = get_model('rbacorg')
    RbacScheme = get_model('rbacscheme')
    p1 = Perm.get(Perm.c.name=="perm1")
    o1 = RbacOrg.get(RbacOrg.c.name=="org1")
    functions.set_echo(True)
    print list(p1.get_users_ids(org=o1))

test()
test_add_Role_Perm_Rel()
test_query_perm()
